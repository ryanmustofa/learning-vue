import axios from "axios";

let env = import.meta.env

axios.defaults.baseURL = env.VITE_BASE_URL

axios.interceptors.response.use((response) => {
  return response
},(error) => {
  console.log(error.message)
  return Promise.reject(error)
})


export default axios

