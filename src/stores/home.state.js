import { defineStore } from "pinia";

export const stateHomeStore = defineStore('home-state', {
  state: () => ({
    search: "",
  }),
  actions: {
    async setSearch(data) {
      this.search = data
    }
  }
})
